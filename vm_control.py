# -*- coding: utf-8 -*-
import os, os.path
import random
import string
import time
import _mysql
from mako.template import Template
import cherrypy

#Variablles de configuracion
MAX_VMS_IN_SECC = 5
MAX_ALUMNS_IN_SECC = 20

#Regresa el tiempo actual
def getTime():
    """Regresa la fecha y hora actual en formato  %Y-%m-%d %H:%M"""
    time_act = time.strftime(
        "%Y-%m-%d %H:%M",
        time.gmtime(
            time.mktime(
                time.strptime(
                    time.strftime("%Y-%m-%d %H:%M"),
                    "%Y-%m-%d %H:%M"
                )
            ) - 6*60*60 #Se incluye la correccion de la zona horaria TEMPORAL
        )
    )
    return time_act

def dateToUnixTime(d):
    """Convierte una feha en formato %Y-%m-%d %H:%M:%S a tiempo unix
       Parámetros:
           d --> fecha en cadena
       Valor de retorno:
           Segundos de tiempo unix
    """
    retval = time.mktime(
        time.strptime(
            d,
            "%Y-%m-%d %H:%M:%S"
        )
    ) - 6*60*60 #Se incluye la correccion de la zona horaria TEMPORAL
    return retval


class DataBase:
    """Clase que abstrae la base de datos
       Es un envoltorio alrededor de una conección mediante el modulo _mysql"""

    def __init__(self):
        """Constructor
           Inicializa la conección a la base de datos mySQL
        """
        self.db = _mysql.connect(host="localhost", user="a1214336f", passwd="Betting", db="a1214336f")

    def query(self, sql):
        """Query generico
           Abstrae del manejo de excepciones al hacer consultas a la base de datos.
           Parámetros:
               sql --> Código sql para hacer la consulta
           Valor de retorno:
               True en caso de ser una insercion correcta
               Registros en caso de una consulta
               None en caso de un error
        """
        reg = None
        try:
            self.db.query(sql)
            q = self.db.store_result()
            if q:
                reg = q.fetch_row(maxrows=0, how=1)
            else:
                reg = True #Para las inserciones
        except Exception as e:
            raise
        return reg

    def modify(self, *sqls):
        """Especialización de un query
           Utilizado para hacer modificaciones o inserciones de datos, es decir
           cuando no se requieren valores de retorno.
           Parámetros:
               sqls --> Lista o tupla de códigos sql a ejecutar
           Valor de retorno:
               True en caso de ser que todas las inserciones sean correctas
               False en caso ccontrario
         """
        retval = True
        for sql in sqls:
            retval = retval and (self.query(sql) != None)
        return retval
        
    ## Query para obtener un elemento ## Regresan str
    def getOne(self, sql):
        """Especialización de un query
           Utilizado para hacer consultas en las que se requiere solo un registro
           de retorno.
           Parámetros:
               sql --> Código sql a ejecutar
           Valor de retorno:
               Registro en caso de que la consulta sea correcta
               None en caso contrario
         """
        data = self.query(sql)
        if data:
            data = data[0]
            for k,v in data.items():
                if type(v) == bytes:
                    data[k] = v.decode("UTF-8")
        return data
    
    ## Query para obtener un todos llos elementos (un alias de DataBase.query())##
    def getAll(self, sql):
        """Especialización de un query
           Utilizado para hacer consultas en las que se requieren varios registros
           de retorno.
           Parámetros:
               sql --> Código sql a ejecutar
           Valor de retorno:
               Registros en caso de que la consulta sea correcta
               None en caso contrario
         """
        regs =  self.query(sql)
        for data in regs:
            for k,v in data.items():
                #Decodificando si es necesario
                if type(v) == bytes:
                    data[k] = v.decode("UTF-8")
        return regs

    def updateReservacion(self):
        """Actualiza el estado de las resevaciones
           Actualizacion de los estados de las reservaciones, de pendiente a no_atendida
           o proceso a atendida si es el caso
        """
        act_time = getTime()
        sql = []
        #Para marcar las no atendidas
        sql.append("""UPDATE
                         reservacion
                      SET
                         status="no_atendida"
                      WHERE
                         fin<="%s" AND
                         status="pendiente"
                   ;""" % (act_time, )
        )
        #Para marcar las atendidas
        sql.append("""UPDATE
                         reservacion
                      SET
                         status="atendida"
                      WHERE
                         fin<="%s" AND
                         status="proceso"
                   ;""" % (act_time, )
        )
        return self.modify(*sql)

    def onInitProcess(self):
        """Procesos de inicio
           Actualizacion de los estados de las reservaciones, es ejecutado despues
           de hacer login, se pueden adgregar nuevos procedimientos de inicio
        """
        return self.updateReservacion()
    
    ##### Consulta (True/False) #####

    def checkUser(self, nickname, password):
        """Verificacion de datos de login
           Verifica que los datos proporcionados existan en la tabla de usuarios
           Parámetros:
               nickname --> Nick de usuario
               password --> Contraseña de usuario
           Valor de retorno:
               True si existe la convinacion nick-pass en la tabla de usuarios
               False en caso contrario
        """
        sql = """SELECT
                    password
                 FROM
                    usuarios
                 WHERE
                    nickname="%s"
              ;""" % (nickname,)
        data = self.getOne(sql)
        #print("Data: ", data)
        if data and data["password"] == password:
            result = True
        else:
            result = False
        return result

    def checkDisponibilidadVM(self, id_vm, ini_res, fin_res):
        """Verificación de disponibilidad de uso de una VM
           Consulta en la tabla de reservaciones para saber si una VM estará disponible
           para todo el periodo consultado
           Parámetros:
               id_vm   --> Id de la máquina virtual 
               ini_res --> Fecha y hora de inicio del posible uso
               fin_res --> Fecha y hora de fin del posible uso
           Valor de retorno:
               True si esta disponible la VM en el periodo solicitado
               False en caso contrario
        """
        sql = """SELECT
                    COUNT(*) AS nInter
                 FROM
                    reservacion AS r
                 WHERE
                    id_vm=%d AND
                    "%s"<fin AND
                    "%s">ini AND
                    (
                       r.status="pendiente" OR
                       r.status="proceso" OR
                       r.status="no_atendida"
                    )
              ;""" % (id_vm, ini_res, fin_res)
        data = self.getOne(sql)
        print "Check disp:", str(data), "\n"
        return not (data and int(data["nInter"])>0)
    
    ##### Obtencion de datos#####    

    def getUserData(self, nickname):
        """Obtiene los datos de un usuario a partir de su nickname
           Parámetros:
               nickname --> Nombre de usuario para la consulta
           Valor de retorno:
               Registro del usuario solicitado si existe
               None en caso contrario
        """
        sql = """SELECT
                    *
                 FROM
                    usuarios
                 WHERE
                    nickname="%s"
              ;""" % (nickname,)
        return self.getOne(sql)

    def getUsers(self):
        """ Obtiene todos los registros de la tabla de usuarios
            Valor de retorno:
                Tupla con todos los registros de usuarios
        """
        sql = """SELECT
                    *
                 FROM
                    usuarios
              ;"""
        return self.getAll(sql)
    
    def getSecciones(self):
        """ Obtiene todos los registros de la tabla de secciones
            Valor de retorno:
                Tupla con todos los registros de secciones
        """
        sql = """SELECT
                    *
                 FROM
                    secciones
              ;"""
        return self.getAll(sql)

    def getProfs(self):
        """ Obtiene todos los registros de la tabla de usuarios que son tipo "prof"
            Valor de retorno:
                Tupla con todos los registros de usuarios tipo profesor
        """
        sql = """SELECT
                    *
                 FROM
                    usuarios
                 WHERE
                    tipo="prof"
              ;"""
        return self.getAll(sql)
    
    def getVMs(self):
        """ Obtiene todos los registros de la tabla de vm (Virtual machines)
            Valor de retorno:
                Tupla con todos los registros de vm
        """
        sql = """SELECT
                    vm.id AS id,
                    s.nota AS seccion,
                    vm.status AS status,
                    dir_red
                 FROM
                    vm,
                    secciones AS s
                 WHERE
                    vm.id_seccion=s.id
              ;"""
        return self.getAll(sql)

    def getSolicitudes(self, status):
        """ Obtiene todos los registros de la tabla de solicitudes con cierto estado
            Parámetros:
                status --> Estado de las solicitudes requeridas
            Valor de retorno:
                Tupla con todos los registros de solicitud_vm que cumplan con tener dicho estado
        """
        sql = """SELECT
                    svm.id AS id,
                    sec.nota as seccion,
                    sec.id AS id_secc,
                    svm.tipo AS tipo,
                    svm.cuantas_vm AS cuantas_vm
                 FROM
                    solicitud_vm as svm,
                    secciones AS sec
                 WHERE
                    sec.id=svm.id_seccion AND
                    svm.status="%s"
              ;""" % (status,)
        return self.getAll(sql)

    def getSeccionesLibres(self):
        """Obtiene las secciones que no tienen profesor
           Valor de retorno:
               Registros con las secciones libres
        """
        sql = """SELECT
                    secciones.id AS id,
                    nota
                 FROM
                    secciones
                 LEFT JOIN
                    profesor_seccion
                 ON
                    secciones.id=profesor_seccion.id_seccion
                 WHERE
                    profesor_seccion.id_seccion IS NULL
              ;"""
        return self.getAll(sql)

    def getSeccionesOcupadas(self):
        """Obtiene las secciones que tienen profesor
           Valor de retorno:
               Registros con las secciones ocupadas
        """
        sql = """SELECT
                    secc.id AS id,
                    nota
                 FROM
                    secciones AS secc,
                    profesor_seccion AS p_s
                 WHERE
                    p_s.id_seccion=secc.id
              ;"""
        return self.getAll(sql)
    
    def getSeccionesDeProf(self, prof):
        """Obtiene las secciones de un dado profesor
           Parámetros:
               prof --> niickname del profesor al cual obtener sus solicitudes
           Valor de retorno:
               Registros con las secciones de dicho profesor
        """
        sql = """SELECT
                    s.id AS id,
                    s.nota AS nota
                 FROM
                    secciones AS s,
                    profesor_seccion AS ps
                 WHERE
                    s.id=ps.id_seccion AND
                    ps.nickname_user="%s"
              ;""" % (prof,)
        return self.getAll(sql)

    def getVMsDeProf(self, prof):
        """Obtiene las vms de las secciones de un dado profesor
           Parámetros:
               prof --> niickname del profesor al cual obtener sus vms
           Valor de retorno:
               Registros con las vms a cargo de dicho profesor
        """
        sql = """SELECT
                    vm.id AS id,
                    s.nota AS seccion,
                    vm.dir_red AS dir_red
                 FROM
                    secciones AS s,
                    profesor_seccion AS ps,
                    vm
                 WHERE
                    ps.nickname_user="%s" AND
                    s.id=ps.id_seccion AND
                    vm.id_seccion=s.id
              ;""" % (prof, )
        return self.getAll(sql)

    def getVMsDeAlumn(self, alumn):
        """Obtiene las secciones que puede usar un alumno
           Parámetros:
               alumn --> niickname del alumno al cual obtener las vms que puede usar
           Valor de retorno:
               Registros con las vms que puede usar el alumno
        """
        sql = """SELECT
                    vm.id AS id,
                    s.nota AS seccion,
                    vm.dir_red AS dir_red
                 FROM
                    secciones AS s,
                    alumno_seccion AS a_s,
                    vm
                 WHERE
                    a_s.nickname_user="%s" AND
                    s.id=a_s.id_seccion AND
                    vm.id_seccion=s.id
              ;""" % (alumn, )
        return self.getAll(sql)

    def getVMsDeAlumnStatus(self, alumn, status):
        """Obtiene las secciones que puede usar un alumno y además están en un estado dado
           Parámetros:
               alumn --> nickname del alumno al cual obtener las vms que puede usar
           Valor de retorno:
               Registros con las vms que puede usar el alumno y que tienen dicho estado
        """
        sql = """SELECT
                    vm.id AS id,
                    s.nota AS seccion,
                    vm.dir_red AS dir_red
                 FROM
                    secciones AS s,
                    alumno_seccion AS a_s,
                    vm
                 WHERE
                    a_s.nickname_user="%s" AND
                    s.id=a_s.id_seccion AND
                    vm.id_seccion=s.id AND
                    vm.status="%s"
              ;""" % (alumn, status)
        return self.getAll(sql)

    
    def getVMsDeProfStatus(self, prof, status):
        """Obtiene las vms que están a cargo de un profesor y además están en un estado dado
           Parámetros:
               prof   --> nickname del profesor al cual obtener las vms que están a su cargo
               status --> estado de las vms a obtener
           Valor de retorno:
               Registros con las vms
        """
        sql = """SELECT
                    vm.id AS id,
                    s.nota AS seccion,
                    vm.dir_red AS dir_red
                 FROM
                    secciones AS s,
                    profesor_seccion AS ps,
                    vm
                 WHERE
                    s.id=ps.id_seccion AND
                    ps.nickname_user="%s" AND
                    vm.id_seccion=s.id AND
                    vm.status="%s"
              ;""" % (prof, status)
        return self.getAll(sql)

    def getLog(self):
        """Obtiene la tabla de log completa
           Valor de retorno:
               Registros de log
        """
        sql = """SELECT
                    *
                 FROM
                    log
              ;"""
        return self.getAll(sql)

    def getReservaciones(self):        
        """Obtiene las reservaciones indicaando ademas la nota de la seccion en la
           que se hizo la reservacion en lugar del id de la misma que está en la tabla
           Valor de retorno:
               Registros de todas las reservaciones
        """
        sql = """SELECT
                    r.id AS id,
                    r.nickname_user AS nickname_user,
                    r.id_vm AS id_vm,
                    sec.nota AS seccion,
                    r.ini AS ini,
                    r.fin AS fin,
                    r.status AS status
                 FROM
                    reservacion AS r,
                    vm,
                    secciones AS sec
                 WHERE
                    r.id_vm=vm.id AND
                    sec.id=vm.id_seccion
              ;"""
        return self.getAll(sql)

    def getUsoVMs(self, nick_prof):
        """Obtiene las reservaciones cuyo estado sea "atendida" o en "proceso"
           y la vm esté a cargo de un profesor dado
           Parámetros:
               nick_prof --> nickname del profesor
           Valor de retorno:
               Registros de reservaciones que cumplan con los requerimientos
        """
        sql = """SELECT
                    r.id AS id,
                    r.nickname_user AS nickname_user,
                    r.id_vm AS id_vm,
                    sec.nota AS seccion,
                    r.ini AS ini,
                    r.fin AS fin
                 FROM
                    reservacion AS r,
                    vm,
                    secciones AS sec,
                    profesor_seccion AS p_s
                 WHERE
                    p_s.id_seccion=sec.id AND
                    p_s.nickname_user="%s" AND
                    r.id_vm=vm.id AND
                    sec.id=vm.id_seccion AND
                    (
                       r.status="atendida" OR
                       r.status="proceso"
                    )
              ;""" % (nick_prof, )
        return self.getAll(sql)

    def getUsoFiltradoVM(self, id_vm, date_ini, date_fin):
        """Obtiene el uso (reservaciones) filtrado por vm en un periodo
           Parámetros:
               id_vm --> id de la vm
               date_ini --> momento de inicio
               date_fin --> momento de fin
           Valor de retorno:
               Registros que cumplan con las restricciones
        """
        sql = """SELECT
                    r.id AS id,
                    r.nickname_user AS nickname_user,
                    r.id_vm AS id_vm,
                    sec.nota AS seccion,
                    r.ini AS ini,
                    r.fin AS fin
                 FROM
                    reservacion AS r,
                    vm,
                    secciones AS sec
                 WHERE
                    r.id_vm=%d AND
                    "%s"<r.fin AND
                    "%s">r.ini AND
                    r.id_vm=vm.id AND
                    sec.id=vm.id_seccion AND
                    (
                       r.status="atendida" OR
                       r.status="proceso"
                    )
              ;""" % (id_vm, date_ini, date_fin)
        return self.getAll(sql)

    def getUsoFiltradoPorAlumno(self, nick_alumn, date_ini, date_fin):
        """Obtiene el uso (reservaciones) filtrado por alumno en un periodo
           Parámetros:
               nick_alumn --> nickname del alumno
               date_ini   --> momento de inicio
               date_fin   --> momento de fin
           Valor de retorno:
               Registros que cumplan con las restricciones
        """
        sql = """SELECT
                    r.id AS id,
                    r.nickname_user AS nickname_user,
                    r.id_vm AS id_vm,
                    sec.nota AS seccion,
                    r.ini AS ini,
                    r.fin AS fin
                 FROM
                    reservacion AS r,
                    vm,
                    secciones AS sec
                 WHERE
                    r.nickname_user="%s" AND
                    "%s"<r.fin AND
                    "%s">r.ini AND
                    r.id_vm=vm.id AND
                    sec.id=vm.id_seccion AND
                    (
                       r.status="atendida" OR
                       r.status="proceso"
                    )
              ;""" % (nick_alumn, date_ini, date_fin)
        return self.getAll(sql)


    def calcHorasUso(self, data):
        """Calcula las horas de uso a partir de los registros de las reservaciones
           Parámetros:
               data --> tupla con las reservacionnes
           Valor de retorno:
               Total de horas de uso
        """
        total = 0
        if data:
            for reg in data:
                seg_ini = dateToUnixTime(reg["ini"]) 
                seg_fin = dateToUnixTime(reg["fin"])
                total += seg_fin - seg_ini
        return total * 1.0 / 3600.0

    def getVMsInSecc(self, id_secc):
        """Obtiene todas las VMs de una seccion dada
           Parámetros:
               id_secc --> id de la seccion
           Valor de retorno:
               Registros con las VMs en la seccion
        """
        sql = """SELECT
                    *
                 FROM
                    vm
                 WHERE
                    id_seccion="%s"
              ;""" % (id_secc,)
        return self.getAll(sql)
   
    def getNumVMsInSecc(self, id_secc):
        """Obtiene el numero de VMs en una seccion
           Parámetros:
               id_secc --> id de la seccion
           Valor de retorno:
               Total de VMs en la seccion
        """
        sql = """SELECT
                    COUNT(*) AS cuenta
                 FROM
                    vm,
                    secciones AS s
                 WHERE
                    vm.id_seccion="%s" AND
                    s.id="%s"
              ;""" % (id_secc, id_secc)
        data = self.getOne(sql)
        if data:
            retval = int(data["cuenta"])
        else:
            retval = MAX_VMS_IN_SECC + 1
        return retval

    def getSolicitud(self, id_sol):
        """Obtiene el registro completo de una solicitud a partir de su id
           Parámetros:
               id_sol --> id de la solicitud
           Valor de retorno:
               Registro de dicha solicitud si existe
        """
        sql = """ SELECT
                     *
                  FROM
                     solicitud_vm
                  WHERE
                     id=%d
              ;""" % (id_sol,)
        return self.getOne(sql)

    def getReservacionesDeUserStatus(self, nickname, status):
        """Obtiene las reservaciones de un usuario en un determinado estado
           Parámetros:
               nickname --> nickname del alumno
               status   --> estado de las reservaciones requeridas
           Valor de retorno:
               Registros de reservaciones
        """
        sql = """SELECT
                    r.id AS id,
                    s.nota AS seccion,
                    vm.id AS id_vm,
                    r.ini AS ini,
                    r.fin AS fin
                 FROM                 
                    reservacion AS r,
                    secciones AS s,
                    vm
                 WHERE
                    r.nickname_user="%s" AND
                    s.id=vm.id_seccion AND
                    r.id_vm=vm.id AND
                    r.status="%s"
              ;""" % (nickname, status)
        return self.getAll(sql)

    def getReservacionesUsablesUser(self, nickname):
        """Obtiene las reservaciones de un usuario que ya se pueden usar
           Parámetros:
               nickname --> nickname del alumno
           Valor de retorno:
               Registros de reservaciones
        """
        act_time = getTime()
        sql = """SELECT
                    r.id AS id,
                    s.nota AS seccion,
                    vm.id AS id_vm,
                    r.ini AS ini,
                    r.fin AS fin
                 FROM 
                    reservacion AS r,
                    secciones AS s,
                    vm
                 WHERE
                    r.nickname_user="%s" AND
                    s.id=vm.id_seccion AND
                    r.id_vm=vm.id AND
                    r.status="pendiente" AND
                    "%s">=r.ini AND
                    "%s"<=r.fin
              ;""" % (nickname, act_time, act_time)
        return self.getAll(sql)

    def numAlumnsInSecc(self, id_secc):
        """Obtiene el número de alumnos en una sección 
           Parámetros:
               id_secc --> id de la seccion a calcular el numero de alumnos
           Valor de retorno:
               Entero con el número de alumnos
        """
        sql =  """SELECT
                     COUNT(*) AS num
                  FROM
                     alumno_seccion
                  WHERE
                     id_seccion=%d
               ;""" % (id_secc, )
        data = self.getOne(sql)
        if data:
            retval = int(data["num"])
        else:
            retval = MAX_ALUMNS_IN_SECC + 1
        return retval

    def getAlumnosDeProf(self, nick_prof):
        """Obtiene todos los alumnos a cargo de un profesor
           Parámetros:
               nick_prof --> nickname del profesor
           Valor de retorno:
               Registros de alumnos
        """
        sql = """SELECT
                    a_s.nickname_user as nickname,
                    s.nota AS seccion
                 FROM profesor_seccion AS p_s,
                    alumno_seccion AS a_s,
                    secciones AS s
                 WHERE
                    s.id=p_s.id_seccion AND
                    s.id=a_s.id_seccion AND
                    p_s.nickname_user="%s" 
              ;""" % (nick_prof, )
        return self.getAll(sql)
    
    ##### Insercion de datos (return (True / False))#####

    def log(self, nick_user, id_vm, suceso, nota):
        """Agrega un evento al log de profesores
           Parámetros:
               nick_user --> nickname del usuario que generó el evento
               id_vm --> id de la máquina virtual
               sucesso --> tipo de acontecimiento
               nota --> Nota informativa
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        act_time = getTime()
        sql = """INSERT INTO
                    log(
                       nickname_user,
                       id_vm,
                       t,
                       suceso,
                       nota
                    )          
                 VALUES(
                    "%s",
                    %d,
                    "%s",
                    "%s",
                    "%s"
                 )
              ;""" % (nick_user, id_vm, act_time, suceso, nota)
        return self.modify(sql)
    
    def addUser(self, nickname, password, nombre, tipo):
        """Agrega un usuario a la tabla de usuarios
           Parámetros:
               nickname --> nickname del nuevo usuario
               password --> contraseña
               nombre --> nommbre del nuevo usuario
               tipo --> tipo de usuario
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """INSERT INTO
                    usuarios
                 VALUES(
                    "%s",
                    "%s",
                    "%s",
                    "%s"
                 )
              ;""" % (nickname, password, nombre, tipo)
        return self.modify(sql)

    def addSeccion(self, nota):
        """Agrega una sección
           Parámetros:
               nota --> Nota descriptiva para esa seccción
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """INSERT INTO
                    secciones(
                       nota
                    )
                 VALUES(
                    "%s"
                 )
              ;""" % (nota,)
        return self.modify(sql)
    
    def crearVMs(self, id_secc, cantidad):
        """Crea el número de VMs especificado en una seccción siempre y cuando
           no se haya alcanzado el límite
           Parámetros:
               id_secc --> id de la sección
               cantidad --> número de máquinas virtuales a crear
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        numVMs = self.getNumVMsInSecc(id_secc)
        retval = False
        if cantidad <= (MAX_VMS_IN_SECC - numVMs):
            sql = """INSERT INTO
                        vm(
                           id_seccion,
                           dir_red,
                           status
                        )
                     VALUES(
                        %d,
                        "0.0.0.0",
                        "libre")
                  ;""" % (id_secc,)
            retval = True
            for k in range(cantidad):
                retval = retval and self.modify(sql)
        return retval

    def asignarProfSecc(self, nickProf, id_secc):
        """Asigna un profesor a una sección
           Parámetros:
               nickProf --> nickname del profesor
               id_secc --> id de la sección
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """INSERT INTO
                    profesor_seccion
                 VALUES(
                    "%s",
                    "%s"
                 )
              ;""" % (nickProf, id_secc)
        return self.modify(sql)

    def addAlumnSecc(self, nickAlumn, id_secc):
        """Registra un alumno en una sección
           Parámetros:
               nickAlumn --> nickname del alumno
               id_secc --> id de la sección
           Valor de retorno:
               Booleano indicando el éxito de la operación
        """
        sql = """INSERT INTO
                    alumno_seccion
                 VALUES(
                    "%s",
                    %d
                 )
              ;""" % (nickAlumn, id_secc)
        return self.modify(sql)

    def addSolicitudVM(self, id_seccion, tipo, cuantas_vm):
        """Agrega una solicitud de VMs
           Parámetros:
               id_seccion --> id de la sección
               tipo --> tipo de solicitud ("crear" o "eliminar")
               cuantas_vm --> número de VMs
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """INSERT INTO
                    solicitud_vm(
                       id_seccion,
                       tipo,
                       cuantas_vm,
                       status
                    )
                 VALUES(
                    %d,
                    "%s",
                    %d,
                    "pendiente"
                 )
              ;""" % (id_seccion, tipo, cuantas_vm)
        return self.modify(sql)
    
    def atenderSolicitudVM(self, id_sol):
        """Trata de atender una soicitud de VMs
           Parámetros:
               id_sol --> id de la solicitud
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        solicitud = self.getSolicitud(id_sol)
        if not solicitud:
            return False
        id_secc = int(solicitud["id_seccion"])
        cuantas_vm = int(solicitud["cuantas_vm"])
        tipo = solicitud["tipo"]
        vms_in_secc = self.getNumVMsInSecc(id_secc)
        if tipo == "crear" and self.crearVMs(id_secc, cuantas_vm) or \
           tipo == "eliminar" and self.delVMs(id_secc, cuantas_vm):
            #Marcando de atendida
            sql = """UPDATE
                        solicitud_vm
                     SET
                        status="atendida"
                     WHERE
                        id=%d
                  ;""" % (id_sol,)
            return self.modify(sql)
        else:
            sql = """UPDATE
                        solicitud_vm
                     SET
                        status="fallida"
                     WHERE
                        id=%d
                  ;""" % (id_sol,)
            return self.modify(sql)
        return False

    def rechazarSolicitudVM(self, id_sol):
        """Trata de rechazar una soicitud de VMs
           Parámetros:
               id_sol --> id de la solicitud
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """UPDATE
                    solicitud_vm
                 SET
                    status="rechazada"
                 WHERE
                    id=%d
              ;""" % (id_sol,)
        return self.modify(sql)

    def reactivarSolicitudVM(self, id_sol):
        """Trata de reactivar una soicitud de VMs
           Parámetros:
               id_sol --> id de la solicitud
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """UPDATE
                    solicitud_vm
                 SET
                    status="pendiente"
                 WHERE
                    id=%d
              ;""" % (id_sol,)
        return self.modify(sql)
    
    def addReservacion(self, nickname, vm_id, datetime_ini, tiempo):
        """Agrega una reservación
           Parámetros:
               nickname --> nickname del alumno
               vm_id --> id de la VM
               datetime_ini --> momento de inicio
               datetime_fin --> momento de fin
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        datetime_actual = getTime()
        print datetime_actual
        if datetime_ini > datetime_actual: #REVIZAR
            datetime_fin = time.strftime(
                "%Y-%m-%d %H:%M:%S",
                time.gmtime(
                    time.mktime(
                        time.strptime(
                            datetime_ini,
                            "%Y-%m-%d %H:%M"
                        )
                    ) + tiempo*60 - 6*60*60 #Se incluye la correccion de la zona horaria TEMPORAL
                )
            )
            if self.checkDisponibilidadVM(vm_id, datetime_ini, datetime_fin):
                sql = """INSERT INTO
                            reservacion(
                               nickname_user,
                               id_vm,
                               ini,
                               fin,
                               status
                            )
                         VALUES(
                            "%s",
                            %d,
                            "%s",
                            "%s",
                            "pendiente"
                         )
                      ;""" % (nickname, vm_id, datetime_ini, datetime_fin)
                print "Esta disponible"
                return self.modify(sql)
        return False

    def cancelarReservacionVM(self, id_res):
        """Trata de cancelar una reservación de VM
           Parámetros:
               id_res --> id de la reservación
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """UPDATE
                    reservacion
                 SET
                    status="cancelada"
                 WHERE
                    id=%d
              ;""" % (id_res, )
        return self.modify(sql)
    
    def apagarVM(self, id_vm):
        """Trata de apagar una VM
           Parámetros:
               id_vm --> id de la VM
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """UPDATE
                    vm
                 SET
                    status="apagada"
                 WHERE
                    id=%d
              ;""" % (id_vm,)
        return self.modify(sql) 

    def encenderVM(self, id_vm):
        """Trata de encender una VM
           Parámetros:
               id_vm --> id de la VM
           Valor de retorno:
               Booleano indicando el éxito de la operación
        """
        sql = """UPDATE
                    vm
                 SET
                    status="libre"
                 WHERE
                    id=%d
              ;""" % (id_vm,)
        return self.modify(sql) 

    def reiniciarVM(self, id_vm):
        """Trata de reiniciar una VM
           Por el momento no hace nada en la base de datos
           Parámetros:
               id_vm --> id de la VM
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        return True

    def clonarVM(self, id_vm):
        """Trata de clonar una VM
           Por el momento no hace nada en la base de datos
           Parámetros:
               id_vm --> id de la VM
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        return True

    def cambiarDirRedVM(self, id_vm, dir_red):
        """Trata de cambiar la direccion de red de una VM
           Parámetros:
               id_vm --> id de la VM
               dir_red --> Nueva dirección de red
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """UPDATE
                    vm
                 SET
                    dir_red="%s"
                 WHERE
                    id=%d
              ;""" % (dir_red, id_vm)
        return self.modify(sql)

    #Usa la reservacion, supone que es usable
    def usarReservacion(self, id_res):
        """Trata de usar una reservación
           Parámetros:
               id_res --> id de la reservación
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """UPDATE
                    reservacion AS r,
                    vm
                 SET
                    r.status="proceso",
                    vm.status="usandose"
                WHERE
                    r.id=%d AND
                    r.id_vm=vm.id
              ;""" % (id_res, )
        return self.modify(sql)

    #Marca como usandose una vm
    def usarVM(self, id_vm):
        """Trata de usar una VM
           Parámetros:
               id_vm --> id de la VM
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """UPDATE 
                    vm       
                 SET
                    status = "usandose"
                 WHERE
                    id=%d
              ;""" % (id_vm, )
        return self.modify(sql)
    
    ##### Eliminacion de datos (return (True / False))#####

    def delUser(self, nick):
        """Trata de eliminar un usuario y todo lo relacionado a el
           Parámetros:
               nick --> nickname del usuario
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = []
        userData = self.getUserData(nick)
        if userData["tipo"] == "alumn":
            sql.append(
                """DELETE FROM
                      alumno_seccion
                   WHERE
                      nickname_user="%s"
                ;""" % (nick,)
            )
        elif userData["tipo"] == "prof":
            sql.append(
                """DELETE FROM
                      profesor_seccion
                   WHERE
                      nickname_user="%s"
                ;""" % (nick,)
            )
        sql.append(
              """DELETE FROM
                    usuarios
                 WHERE
                    nickname="%s"
              ;""" % (nick,)
        )
        return self.modify(*sql)

    def delVM(self, id_vm):
        """Trata de eliminar una VM y todo lo relacionado a ella
           Parámetros:
               id_vm --> id de la VM
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = []
        sql.append("""DELETE FROM
                         vm
                      WHERE
                         id=%d
                   ;""" % (id_vm,)
        )
        sql.append("""DELETE FROM
                         reservacion
                      WHERE
                         id_vm=%d
                   ;""" % (id_vm,)
        )
        return self.modify(*sql)

    def delVMs(self, id_secc, cuantas_vm):
        """Trata de eliminar VMs de una sección
           Parámetros:
               id_secc --> id de la sección
               cuantas_vm -->  número de VMs a eliminar
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        vmsInSecc = self.getVMsInSecc(id_secc)
        if vmsInSecc and cuantas_vm <= len(vmsInSecc): #Si se puede eliminar ese numero de maquinas virtuales
            for k in range(cuantas_vm):
                self.delVM(int(vmsInSecc[k]["id"]))
            return True
        return False
    
    def delProfSecc(self, id_secc):
        """Trata de quitar un profesor a una sección
           Parámetros:
               id_secc --> id de la sección
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        sql = """DELETE FROM
                    profesor_seccion
                 WHERE
                    id_seccion=%d
              ;""" % (id_secc,)
        return self.modify(sql)
    
    def delSeccion(self, id_secc):
        """Trata de eliminar una sección y todo lo relacionado a ella 
           Parámetros:
               id_secc --> id de la sección
           Valor de retorno:
               Booleano indicando el exito de la operación
        """
        #Eliminando los alumnos de la seccion
        sql = []
        sql.append("""DELETE FROM
                            alumno_seccion
                         WHERE
                            id_seccion=%d
                      ;""" % (id_secc,))
        #Eliminando las solicitudes del profesor de la seccion
        sql.append("""DELETE FROM
                            solicitud_vm
                         WHERE
                            id_seccion=%d
                      ;""" % (id_secc,))
        #Eliminando las reservaciones de las vms de la seccion
        data = self.getVMsInSecc(id_secc)
        for reg in data:
            sql.append("""DELETE FROM
                                reservacion
                             WHERE
                                id_vm=%d
                          ;""" % (int(reg["id"]),))
        #Eliminando las vms de la seccion
        sql.append("""DELETE FROM
                            vm
                         WHERE
                            id_seccion=%d
                      ;""" % (id_secc,))
        #Eliminando la seccion
        sql.append("""DELETE FROM
                            secciones
                         WHERE
                            id=%d
                      ;""" % (id_secc,))
        return self.modify(*sql)
    
    def close(self):
        """Cierra la connección con la base de datos"""
        self.db.close()

        
#Para simular variables estáticas
def static_vars(**kwargs):
    """Se usa como envoltorio de ejecutables para provveer de la
       funcionalidad de variables estáticas en métodos la cual no se encuentra
       implementada por defecto en python
    """
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate
        
#Metodo para obtener la database unica
@static_vars(db=DataBase())
def getDB():
    """Regresa el unico objeto de conección a la base de datos (DataBase)
       Valor de retorno:
           Objeto DataBase
    """
    return getDB.db


class VMControl:
    """Clase principal para la aplicacion web"""
    
    def __init__(self):
        """Constructor
           Obtiene el objeto de coneccion con la base de datos y carga las plantillas html
        """
        self.db = getDB()
        self.templates = dict()
        # Para e login
        self.templates["login"] = Template(filename="static/html/login.html")
        # Panel de control
        self.templates["panel"] = Template(filename="static/html/panel.html")
        #Valores para los paneles de control
        self.panelActions = dict()
        #Acciones del administrador
        self.panelActions["admin"] = (
            ("crear_usuario", "Crear usuario"),
            ("eliminar_usuario", "Eliminar usuario"),
            ("crear_vms", "Crear VMs"),
	    ("eliminar_vms", "Eliminar VMs"),
	    ("crear_seccion", "Crear Seccion"),
	    ("eliminar_seccion", "Eliminar Seccion"),
	    ("asignar_prof_secc", "Asignar Profesor a Seccion"),
	    ("eliminar_prof_secc", "Quitar Profesor a Seccion"),
	    ("mostrar_reporte", "Reporte de uso"),
	    ("mostrar_solicitudes", "Mostrar solicitudes")
        )
        #Acciones del profesor
        self.panelActions["prof"] = (
            ("crear_alumn", "Crear usuario alumno"),
            ("encender_vm", "Encender VM"),
            ("apagar_vm", "Apagar VM"),
            ("reiniciar_vm", "Reiniciar VM"),
            ("clonar_vm", "Clonar VM"),
            ("cambiar_dir_red_vm", "Cambiar direccion de red de VM"),
            ("reporte_uso_por_alumno", "Reporte de uso por alumno"),
            ("reporte_uso_vm", "Reporte de uso de VMs"),
            ("solicitud_vm", "Solicitud VMs")
        )
        self.panelActions["alumn"] = (
            ("reservar_vm", "Reservar VM"),
            ("cancelar_reservacion_vm", "Cancelar reservacion de VM"),
            ("usar_vm", "Usar VM"),
            #("liberar_vm", "Liberar VM")
        )
        # Construyendo los templates de las acciones
        for actions in self.panelActions.values():
            #Construyendolo
            for act in actions:
                k = act[0]
                self.templates[k] = Template(filename="static/html/"+ k +".html")
        
    @cherrypy.expose
    def index(self, **kwargs):
        """Sitio principal desde donde se redirecciona al login o al panel"""
        #Si ya inicio sesión
        if "loged" in cherrypy.session.keys() and cherrypy.session["loged"]:
            #Se redirecciona al panel
            raise cherrypy.HTTPRedirect("/panel/")
        else: #Si no ha iniciado sesion
            #Se redirecciona a la pagina de login
            raise cherrypy.HTTPRedirect("/login/")
    
    @cherrypy.expose
    def login(self, **kwargs): # Login de inicio
        """Muestra la pagina de inicio de sesion si aun no se está logueado
           en caso contrario redirige al panel (visible, con seguridad)
           Parámetros:
               **kwargs --> Cualquier parametro pasado por POST o GET,
                            si no se necesitan son ignorados
           Valor de retorno:
               Pagina de login si no se está logueado
        """
        if "login" in kwargs.keys(): #Si ya se enviaron los datos de usuario
            #Verificar que los datos son correctos
            #print(kwargs["nickname"], kwargs["password"],"\n\n")
            if self.db.checkUser(kwargs["nickname"], kwargs["password"]): #Si son correctos
                #Se copian los datos utiles a la sesion
                data = self.db.getUserData(kwargs["nickname"])
                for k,v in data.items():
                    cherrypy.session[k] = v#.decode("UTF-8")
                #Poniendo el loged en True
                cherrypy.session["loged"] = True
                #Se hace el proceso de inicio
                self.db.onInitProcess()
                #Se redirecciona al index y de ally al panel de control
                raise cherrypy.HTTPRedirect("/index/")
            else:
                return self.templates["login"].render(msg = "Usuario o password incorrecta")
        elif "loged" in cherrypy.session:
            #Se redirecciona al panel
            raise cherrypy.HTTPRedirect("/panel/")
        # Se muestra la pagina de login
        return self.templates["login"].render(msg = "")

    @cherrypy.expose
    def panel(self, **kwargs): # Panel de control
        """Muestra un panel generico con las acciones que puede realizar el usuario logueado,
           en caso contrario una pagina de restricción (visible, con seguridad)
           Parámetros:
               **kwargs --> Cualquier parametro pasado por POST o GET,
                            si no se necesitan son ignorados
           Valor de retorno:
               Pagina de restriccion si no se está logueado
               Panel renderizado si se está dentro de la sesión
        """
        result = ""
        #Si ya se hizo login
        if "loged" in cherrypy.session:
            #Se muestra el contenido acorde al usuario
            tipo = cherrypy.session["tipo"]
            result = self.templates["panel"].render(
                tipo_user = cherrypy.session["tipo"],
                nombre = cherrypy.session["nombre"],
                script = "/" + cherrypy.session["tipo"] + "Actions/",
                actions = self.panelActions[cherrypy.session["tipo"]]
            )
        else:
            result = open("./static/html/restricted.html")
        return result

    ############### ACCIONES DE ADMIN ####################

    def crear_usuario(self, **kwargs):
        """Ejecuta la acción de crear un usuario,
           renderiza tambien la página donde se muestra un formulario para
           ingresar los datos del nuevo usuario (no visible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_crear_usuario" in kwargs:
            if "nickname" in kwargs and len(kwargs["nickname"]) and \
               "password" in kwargs and len(kwargs["password"]) and \
               "nombre" in kwargs and len(kwargs["nombre"]) and \
               "tipo" in kwargs and len(kwargs["tipo"]) and \
               self.db.addUser(kwargs["nickname"], kwargs["password"], kwargs["nombre"], kwargs["tipo"]):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["crear_usuario"].render(msg=msg)

    def eliminar_usuario(self, **kwargs):
        """Ejecuta la acción de eliminar un usuario,
           renderiza tambien la página donde se muestran todos las usuarios (no visible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_eliminar_usuario" in kwargs:
            if "nickname" in kwargs and len(kwargs["nickname"]) and \
               self.db.delUser(kwargs["nickname"]):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["eliminar_usuario"].render(
            usuarios = self.db.getUsers(),
            msg=msg
        )    
    
    def crear_vms(self, **kwargs):
        """Ejecuta la acción de crear una o varias VMs para una sección,
           renderiza tambien la página donde se muestra un formulario para
           seleccionar el numero de VMs y a que sección asignarlas (no visible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_crear_vms" in kwargs:
            #print("argumentos del POST",kwargs, sep = "\n")
            if "seccion" in kwargs and \
               "cantidad_vm" in kwargs and \
               self.db.crearVMs(int(kwargs["seccion"]), int(kwargs["cantidad_vm"])):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["crear_vms"].render(secciones = self.db.getSecciones(), msg=msg)

    def eliminar_vms(self, **kwargs):
        """Ejecuta la acción de eliminar una VM,
           renderiza tambien la página donde se muestran todas las VMs que se
           pueden eliminar (no visible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_eliminar_vms" in kwargs:
            if "vm" in kwargs and \
               self.db.delVM(int(kwargs["vm"])):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["eliminar_vms"].render(vms = self.db.getVMs(), msg=msg)

    def crear_seccion(self, **kwargs):
        """Ejecuta la acción de crear una sección,
           renderiza tambien la página donde se muestra un formularrio para
           ingresar los datos de la nueva sección (no visible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_crear_seccion" in kwargs:
            if "nombre_seccion" in kwargs and len(kwargs["nombre_seccion"]) and \
               self.db.addSeccion(kwargs["nombre_seccion"]):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["crear_seccion"].render(msg=msg)   

    def eliminar_seccion(self, **kwargs):
        """Ejecuta la acción de eliminar una seccion,
           renderiza tambien la página donde se muestran todas las secciones (no visible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_eliminar_seccion" in kwargs:
            if "seccion_id" in kwargs and \
               self.db.delSeccion(int(kwargs["seccion_id"])):
                msg = "Operacion exitosa"
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["eliminar_seccion"].render(
            secciones = self.db.getSecciones(),
            msg=msg)

    def asignar_prof_secc(self, **kwargs):
        """Ejecuta la acción de asignar el profesor de una seccion,
           renderiza tambien la página donde se muestran las secciones a las que se les
           puede asignar un profesor (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_asignar_prof_secc" in kwargs:
            if "seccion_id" in kwargs and \
               "nick_prof" in kwargs and \
               self.db.asignarProfSecc(kwargs["nick_prof"], int(kwargs["seccion_id"])):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["asignar_prof_secc"].render(
            secciones = self.db.getSeccionesLibres(),
            profesores = self.db.getProfs(),
            msg=msg)

    def eliminar_prof_secc(self, **kwargs):
        """Ejecuta la acción de quitar el profesor de una seccion,
           renderiza tambien la página donde se muestran las secciones a las que se les
           puede quitar el profesor (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_eliminar_prof_secc" in kwargs:
            if "seccion_id" in kwargs and \
               self.db.delProfSecc(int(kwargs["seccion_id"])):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["eliminar_prof_secc"].render(
            secciones = self.db.getSeccionesOcupadas(),
            msg=msg)
    
    def mostrar_reporte(self, **kwargs):
        """Renderiza la pagina donde se muestran el reporte de uso de VMs tanto
           por alumnos como por profesores
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        #Actualizando el estado de las reservaciones
        self.db.updateReservacion()
        #Renderizando la pagina
        return self.templates["mostrar_reporte"].render(
            log = self.db.getLog(),
            reservaciones = self.db.getReservaciones())
        
    def mostrar_solicitudes(self, **kwargs):
        """Renderiza la pagina donde se muestran las solicitudes de VMs y ejeccuta las
           acciones indicadas por el admin sobre dichas solicitudes (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por adminActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_mostrar_solicitudes" in kwargs:
            if kwargs["click_mostrar_solicitudes"] == "Atender":
                if self.db.atenderSolicitudVM(int(kwargs["id"])):
                    msg = "Solicitud %s atendida" % (kwargs["id"],)
                else:
                    msg = "Solicitud %s no atendida" % (kwargs["id"],)
            elif kwargs["click_mostrar_solicitudes"] == "Rechazar":
                if self.db.rechazarSolicitudVM(int(kwargs["id"])):
                    msg = "Solicitud %s rechazada" % (kwargs["id"],)
                else:
                    msg = "Solicitud %s no rechazada" % (kwargs["id"],)
            elif kwargs["click_mostrar_solicitudes"] == "Reactivar":
                if self.db.reactivarSolicitudVM(int(kwargs["id"])):
                    msg = "Solicitud %s reactivada" % (kwargs["id"],)
                else:
                    msg = "Solicitud %s no reactivada" % (kwargs["id"],)
        #Renderizando la pagina
        return self.templates["mostrar_solicitudes"].render(
            pendientes = self.db.getSolicitudes("pendiente"),
            rechazadas = self.db.getSolicitudes("rechazada"),
            fallidas = self.db.getSolicitudes("fallida"),
            atendidas = self.db.getSolicitudes("atendida"),
            msg=msg)
    
    @cherrypy.expose
    def adminActions(self, **kwargs):
        """Acciones que puede realizar el administrador, utiliza otras funciones no visibles para
           mostrar el contenido apropiado (visible, con seguridad)
           Parámetros:
               **kwargs --> Cualquier parametro pasado por POST o GET,
                            si no se necesitan son ignorados
           Valor de retorno:
               Pagina de restriccion si no se está logueado o no es un administrador
        """
        # solo se muestran las acciones si es un administrador
        if "tipo" in cherrypy.session and cherrypy.session["tipo"] == "admin":
            #Para cada tipo de accion
            if "crear_vms" in kwargs or "click_crear_vms" in kwargs:
                return self.crear_vms(**kwargs)
            elif "eliminar_vms" in kwargs or "click_eliminar_vms" in kwargs:
                return self.eliminar_vms(**kwargs)
            elif "crear_usuario" in kwargs or "click_crear_usuario" in kwargs:
                return self.crear_usuario(**kwargs)
            elif "eliminar_usuario" in kwargs or "click_eliminar_usuario" in kwargs:
                return self.eliminar_usuario(**kwargs)
            elif "crear_seccion" in kwargs or "click_crear_seccion" in kwargs:
                return self.crear_seccion(**kwargs)
            elif "eliminar_seccion" in kwargs or "click_eliminar_seccion" in kwargs:
                return self.eliminar_seccion(**kwargs)
            elif "asignar_prof_secc" in kwargs or "click_asignar_prof_secc" in kwargs:
                return self.asignar_prof_secc(**kwargs)
            elif "eliminar_prof_secc" in kwargs or "click_eliminar_prof_secc" in kwargs:
                return self.eliminar_prof_secc(**kwargs)
            elif "mostrar_reporte" in kwargs:
                return self.mostrar_reporte(**kwargs)
            elif "mostrar_solicitudes" in kwargs or "click_mostrar_solicitudes" in kwargs:
                return self.mostrar_solicitudes(**kwargs)
        return open("./static/html/restricted.html")


    ##################### ACCIONES DE PROFESOR ########################

    def crear_alumn(self, **kwargs):
        """Ejecuta la acción de crear un alumno en una seccion,
           renderiza tambien la página donde se muestran un formulario para ingresar los
           datos de dicho alumno y seleccionar la seccion donde inscribirlo
           puede apagar (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_crear_alumn" in kwargs:
            if "nickname" in kwargs and len(kwargs["nickname"]) and \
               "password" in kwargs and len(kwargs["password"]) and \
               "nombre" in kwargs and len(kwargs["nombre"]) and \
               "id_secc" in kwargs and \
               self.db.numAlumnsInSecc(int(kwargs["id_secc"])) < 20 and \
               self.db.addUser(kwargs["nickname"], kwargs["password"], kwargs["nombre"], "alumn") and \
               self.db.addAlumnSecc(kwargs["nickname"], int(kwargs["id_secc"])):
                msg = "Operacion exitosa" 
            else:
                msg = "Operacion fallida"
        #Renderizando la pagina
        return self.templates["crear_alumn"].render(
            secciones = self.db.getSeccionesDeProf(cherrypy.session["nickname"]),
            msg=msg
        )

    def encender_vm(self, **kwargs):
        """Ejecuta la acción de encender una VM,
           renderiza tambien la página donde se muestran las VMs a las que se les
           puede encender (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_encender_vm" in kwargs:
            if "vm" in kwargs and \
               self.db.encenderVM(int(kwargs["vm"])):
                msg = "VM %d encendida" % (int(kwargs["vm"]), )
                #Haciendo el logging
                self.db.log(cherrypy.session["nickname"],
                            int(kwargs["vm"]),
                            "encender",
                            "Encendido normal")
            else:
                msg = "VM no encendida"
        #Renderizando la pagina
        return self.templates["encender_vm"].render(
            vms = self.db.getVMsDeProfStatus(cherrypy.session["nickname"], "apagada"),
            msg=msg
        )

    def apagar_vm(self, **kwargs):
        """Ejecuta la acción de apagar una VM,
           renderiza tambien la página donde se muestran las VMs a las que se les
           puede apagar (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_apagar_vm" in kwargs:
            if "vm" in kwargs and \
               self.db.apagarVM(int(kwargs["vm"])):
                msg = "VM %d apagada" % (int(kwargs["vm"]), )
                #Haciendo el logging
                self.db.log(cherrypy.session["nickname"],
                            int(kwargs["vm"]),
                            "apagar",
                            "Apagado normal")
            else:
                msg = "VM no apagada"
        #Renderizando la pagina
        return self.templates["apagar_vm"].render(
            vms = self.db.getVMsDeProfStatus(cherrypy.session["nickname"], "libre"),
            msg=msg
        )

    def reiniciar_vm(self, **kwargs):
        """Ejecuta la acción de reiniciar una VM,
           renderiza tambien la página donde se muestran las VMs a las que se les
           puede reinicia (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_reiniciar_vm" in kwargs:
            if "vm" in kwargs and \
               self.db.reiniciarVM(int(kwargs["vm"])):
                msg = "VM %d reiniciada" % (int(kwargs["vm"]), )
                #Haciendo el logging
                self.db.log(cherrypy.session["nickname"],
                            int(kwargs["vm"]),
                            "reiniciar",
                            "Reinicio normal")
            else:
                msg = "VM no reiniciada"
        #Renderizando la pagina
        return self.templates["reiniciar_vm"].render(
            vms = self.db.getVMsDeProfStatus(cherrypy.session["nickname"], "libre"),
            msg=msg)

    def cambiar_dir_red_vm(self, **kwargs):
        """Ejecuta la acción de cambiar la dirección de red a una VM,
           renderiza tambien la página donde se muestran las VMs a las que se les
           puede cambiar la dirección de red  (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_cambiar_dir_red_vm" in kwargs:
            if "vm" in kwargs and \
               "dir_red" in kwargs and len(kwargs["dir_red"]) and \
               self.db.cambiarDirRedVM(int(kwargs["vm"]), kwargs["dir_red"]):
                msg = "Direccion cambiada"
                #Haciendo el logging
                self.db.log(cherrypy.session["nickname"],
                            int(kwargs["vm"]),
                            "cambio_dir_red",
                            "Nueva dir_red: " + kwargs["dir_red"])
            else:
                msg = "Direccion no cambiada"
        #Renderizando la pagina
        return self.templates["cambiar_dir_red_vm"].render(
            vms = self.db.getVMsDeProf(cherrypy.session["nickname"]),
            msg=msg
        )

    def clonar_vm(self, **kwargs):
        """Ejecuta la accion de clonar VM, renderiza tambien la pagina donde
           se muestran las VMs que se pueden clonar (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por prrofActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_clonar_vm" in kwargs:
            if "vm" in kwargs and \
               self.db.clonarVM(int(kwargs["vm"])):
                msg = "VM clonada"
                #Haciendo el logging
                self.db.log(cherrypy.session["nickname"],
                            int(kwargs["vm"]),
                            "clonar",
                            "VM clonada correctamente")
            else:
                msg = "VM no clonada"
        #Renderizando la pagina
        return self.templates["clonar_vm"].render(
            vms = self.db.getVMsDeProf(cherrypy.session["nickname"]),
            msg=msg
        )

    def reporte_uso_por_alumno(self, **kwargs):
        """Renderiza la página donde se muestra el reporte de uso por alumno y eejeccuta los
           filtros solicitados (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        #Actualizando el estado de las reservaciones
        self.db.updateReservacion()
        #
        msg = ""
        uso = ()
        #Si ya se hizo una solicitud
        if "click_reporte_uso_por_alumno" in kwargs:
            print str(kwargs)
            if "nick_alumn" in kwargs and len(kwargs["nick_alumn"]) and \
               "date_ini" in kwargs and len(kwargs["date_ini"]) and \
               "date_fin" in kwargs and len(kwargs["date_fin"]):
                uso = self.db.getUsoFiltradoPorAlumno(kwargs["nick_alumn"],
                                                      kwargs["date_ini"] + " 00:00",
                                                      kwargs["date_fin"] + " 23:59"
                )
                msg = "El alumno uso las VMs: " + \
                      str(self.db.calcHorasUso(uso)) + \
                      " Hrs\n<br><br>Contenido filtrado"
            else:
                uso = self.db.getUsoVMs(cherrypy.session["nickname"])
                msg = "Error al filtrar"
        else:
            uso = self.db.getUsoVMs(cherrypy.session["nickname"])
            msg = "Contenido no filtrado"
        #Renderizando la pagina
        return self.templates["reporte_uso_por_alumno"].render(
            alumns = self.db.getAlumnosDeProf(cherrypy.session["nickname"]),
            uso = uso,
            msg=msg
        )
    
    def reporte_uso_vm(self, **kwargs):
        """Rendeirza la pagina donde se muestra el reporte de uso por vm y eejeccuta los
           filtros solicitados (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        #Actualizando el estado de las reservaciones
        self.db.updateReservacion()
        #
        msg = ""
        uso = ()
        #Si ya se hizo una solicitud
        if "click_reporte_uso_vm" in kwargs:
            print str(kwargs)
            if "vm_id" in kwargs and \
               "date_ini" in kwargs and len(kwargs["date_ini"]) and \
               "date_fin" in kwargs and len(kwargs["date_fin"]):
                uso = self.db.getUsoFiltradoVM(int(kwargs["vm_id"]),
                                            kwargs["date_ini"] + " 00:00",
                                            kwargs["date_fin"] + " 23:59"
                )
                msg = "Contenido filtrado"
            else:
                uso = self.db.getUsoVMs(cherrypy.session["nickname"])
                msg = "Error al filtrar"
        else:
            uso = self.db.getUsoVMs(cherrypy.session["nickname"])
            msg = "Contenido no filtrado"
        #Renderizando la pagina
        return self.templates["reporte_uso_vm"].render(
            vms = self.db.getVMsDeProf(cherrypy.session["nickname"]),
            uso = uso,
            msg=msg
        )
        
    def solicitud_vm(self, **kwargs):
        """Ejecuta la accion de hacer una solicitud de VM, rendeirza tambien la pagina donde
           se muestran las solicitudes que se pueden usar (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por profActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        msg = ""
        #Si ya se hizo una solicitud
        if "click_solicitud_vm" in kwargs:
            if "id_seccion" in kwargs and len(kwargs["id_seccion"]) and \
               "tipo" in kwargs and len(kwargs["tipo"]) and \
               "cuantas_vm" in kwargs  and \
               self.db.addSolicitudVM(int(kwargs["id_seccion"]), kwargs["tipo"], int(kwargs["cuantas_vm"])):
                msg = "Solicitud enviada"
            else:
                msg = "Solicitud no enviada"
        #Renderizando la pagina
        return self.templates["solicitud_vm"].render(
            secciones = self.db.getSeccionesDeProf(cherrypy.session["nickname"]),
            msg=msg
        )

    @cherrypy.expose
    def profActions(self, **kwargs):
        """Acciones que puede realizar el profesor, utiliza otras funciones no visibles para
           mostrar el contenido apropiado (visible, con seguridad)
           Parámetros:
               **kwargs --> Cualquier parametro pasado por POST o GET,
                            si no se necesitan son ignorados
           Valor de retorno:
               Pagina de restriccion si no se está logueado o no es un profesor
        """
        # solo se muestran las acciones si es un profesor
        if "tipo" in cherrypy.session and cherrypy.session["tipo"]=="prof":
            if "crear_alumn" in kwargs or "click_crear_alumn" in kwargs:
                return self.crear_alumn(**kwargs)
            elif "encender_vm" in kwargs or "click_encender_vm" in kwargs:
                return self.encender_vm(**kwargs)
            elif "apagar_vm" in kwargs or "click_apagar_vm" in kwargs:
                return self.apagar_vm(**kwargs)
            elif "reiniciar_vm" in kwargs or "click_reiniciar_vm" in kwargs:
                return self.reiniciar_vm(**kwargs)
            elif "clonar_vm" in kwargs or "click_clonar_vm" in kwargs:
                return self.clonar_vm(**kwargs)
            elif "reporte_uso_por_alumno" in kwargs or "click_reporte_uso_por_alumno" in kwargs:
                return self.reporte_uso_por_alumno(**kwargs)
            elif "reporte_uso_vm" in kwargs or "click_reporte_uso_vm" in kwargs:
                return self.reporte_uso_vm(**kwargs)
            elif "cambiar_dir_red_vm" in kwargs or "click_cambiar_dir_red_vm" in kwargs:
                return self.cambiar_dir_red_vm(**kwargs)
            elif "solicitud_vm" in kwargs or "click_solicitud_vm" in kwargs:
                return self.solicitud_vm(**kwargs)
        return open("./static/html/restricted.html")

    ########### ACCIONES DE ALUMNO ##################

    def reservar_vm(self, **kwargs):
        """Ejecuta la accion de reservar una VM, rendeirza tambien la pagina donde
           se muestra un formulario para hacer la reservacion (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por alumnActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        #Actualizando el estado de las reservaciones
        self.db.updateReservacion()
        msg = ""
        #Si ya se hizo una solicitud
        if "click_reservar_vm" in kwargs:
            if "vm_id" in kwargs and \
               "date_ini" in kwargs and kwargs["date_ini"] and \
               "time_ini" in kwargs and kwargs["time_ini"] and \
               "tiempo" in kwargs and kwargs["tiempo"] and \
               self.db.addReservacion(
                   cherrypy.session["nickname"],
                   int(kwargs["vm_id"]),
                   kwargs["date_ini"] + " " + kwargs["time_ini"],
                   int(kwargs["tiempo"])
               ):
                msg = "Reservacion hecha"
            else:
                msg = "Reservacion fallida, elija otro horario u otra VM"
        return self.templates["reservar_vm"].render(
            vms = self.db.getVMsDeAlumn(cherrypy.session["nickname"]),
            msg=msg
        )

    def cancelar_reservacion_vm(self, **kwargs):
        """Ejecuta la accion de cancelar la reservacion de una VM, renderiza tambien la pagina donde
           se muestran las solicitudes que se pueden cancelar (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por alumnActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        #Actualizando el estado de las reservaciones
        self.db.updateReservacion()
        msg = ""
        #Si ya se hizo una solicitud
        if "click_cancelar_reservacion_vm" in kwargs:
            if "id" in kwargs and \
               self.db.cancelarReservacionVM(int(kwargs["id"])):
                msg = "Reservacion cancelada"
            else:
                msg = "Reservacion no cancelada"
        return self.templates["cancelar_reservacion_vm"].render(
            pendientes = self.db.getReservacionesDeUserStatus(cherrypy.session["nickname"], "pendiente"),
            msg=msg
        )

    def usar_vm(self, **kwargs):
        """Ejecuta la accion de usar una VM, rendeirza tambien la pagina donde
           se muestran las solicitudes que se pueden usar (no viisible)
           Parámetros:
               **kwargs --> parametros pasados por alumnActions para realizar su trabajo
           Valor de retorno:
               Pagina renderizada apropiadamente con los valores recibidos
        """
        #Actualizando el estado de las reservaciones
        self.db.updateReservacion()
        msg = ""
        print "\nargs:" + str(kwargs) + "\n"
        #Si ya se hizo una solicitud
        if "click_usar_vm" in kwargs:
            if "id_res" in kwargs and \
               self.db.usarReservacion(int(kwargs["id_res"])):
                msg = "Reservacion usada"
            else:
                msg = "VM no puede ser usada"
        return self.templates["usar_vm"].render(
            res_usables = self.db.getReservacionesUsablesUser(cherrypy.session["nickname"]),
            msg=msg
        )

    @cherrypy.expose
    def alumnActions(self, **kwargs):
        """Redireccionamiento de todas las acciones que puede realizar un alumno,
           se vale de otras funciones no visibles (visible, con seguridad)
           Parámetros:
               **kwargs --> Cualquier parametro pasado por POST o GET,
                            si no se necesitan son ignorados
           Valor de retorno:
               Pagina de restriccion si no se está logueado o no es un alumno
        """
        # solo se muestran las acciones si es un alumno
        if "tipo" in cherrypy.session and cherrypy.session["tipo"]=="alumn":
            if "reservar_vm" in kwargs or "click_reservar_vm" in kwargs:
                return self.reservar_vm(**kwargs)
            elif "cancelar_reservacion_vm" in kwargs or "click_cancelar_reservacion_vm" in kwargs:
                return self.cancelar_reservacion_vm(**kwargs)
            elif "usar_vm" in kwargs or "click_usar_vm" in kwargs:
                return self.usar_vm(**kwargs)
        return open("./static/html/restricted.html")

    @cherrypy.expose
    def logout(self, **kwargs): #Para salir de la sesion
        """Accion de salir de sesion (visible, con seguridad)
           Parámetros:
               **kwargs --> Cualquier parametro pasado por POST o GET,
                            si no se necesitan son ignorados
           Valor de retorno:
               Pagina de restriccion si no se está logueado
        """
        if "loged" in cherrypy.session:
            cherrypy.session.clear()
            raise cherrypy.HTTPRedirect("/index/")
        else:
            return open("./static/html/restricted.html")
        

def main():
    """Funcion de ejecucion principal, lanza el servidor con la aplicacion"""
    # Configuracion de la aplicacion
    conf = {
        "/" : {
            "tools.sessions.on" : True,
            "tools.staticdir.root": os.path.abspath(os.getcwd())
        }
    }
    #Iniciando la aplicacion
    cherrypy.quickstart(VMControl(), "/", conf)
    
if __name__ == "__main__":
    main()
